# Flectra Community / business-requirement

None



Available addons
----------------

addon | version | summary
--- | --- | ---
[business_requirement_sale_timesheet](business_requirement_sale_timesheet/) | 2.0.1.0.0| Business Requirement Sale Timesheet
[business_requirement_crm](business_requirement_crm/) | 2.0.1.0.0| Convert Leads to Business Requirement
[business_requirement_sale](business_requirement_sale/) | 2.0.1.0.0| Convert Business Requirement into Sales Orders
[business_requirement_deliverable](business_requirement_deliverable/) | 2.0.1.1.0| Manage the Business Requirement Deliverables                 for your customers
[business_requirement](business_requirement/) | 2.0.1.2.0| Manage the Business Requirements (stories, scenarios, gaps        and test cases) for your customers


